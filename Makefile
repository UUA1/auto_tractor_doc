# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = source
BUILDDIR      = build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	# drawio -x localization/localization.drawio -f svg -o localization/img/
	# drawio -x detection/detection.drawio -f svg -o detection/img/
	# drawio -x detection/detection_2.0.drawio -f svg -o detection/img/
	# drawio -x mission_planning/mission_planning.drawio -f svg -o mission_planning/img/
	# drawio -x motion_planning/motion_planning.drawio -f svg -o motion_planning/img/
	# drawio -x motion_planning/pure_pursuit.drawio -f svg -o motion_planning/img/
	# drawio -x map/map.drawio -f svg -o map/img/
	# drawio -x actuation/driver.drawio -f svg -o actuation/img/
	
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
