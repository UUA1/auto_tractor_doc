#!/bin/bash

set -e

# Default settings
CUDA="on"
IMAGE="registry.cn-hangzhou.aliyuncs.com/work_space/autoware.ai:melodic-cuda"

USER_ID="$(id -u)"

# Convert a relative directory path to absolute
echo -e "\tUID: <$USER_ID>"

RUNTIME="--runtime=nvidia"

XSOCK=/tmp/.X11-unix
XAUTH=$HOME/.Xauthority
FONTS=/usr/share/fonts # 字体问题
HOST_CONFIG=$HOME/.config
DOCKER_CONFIG=/home/docker/.config


SHARED_DOCKER_DIR=/home/docker/shared_dir
SHARED_HOST_DIR=$HOME/shared_dir


VOLUMES="--volume=$XSOCK:$XSOCK:rw
         --volume=$XAUTH:$XAUTH:rw
         --volume=$SHARED_HOST_DIR:$SHARED_DOCKER_DIR:rw
         --volume=$HOST_CONFIG:$DOCKER_CONFIG:rw
         --volume=$FONTS:$FONTS:rw" # 字体问题


# Create the shared directory in advance to ensure it is owned by the host user
mkdir -p $SHARED_HOST_DIR/work/src


echo "Launching $IMAGE"

xhost +local:docker

docker run \
    -d \
    --restart=always \
    --name="base-develop" \
    $VOLUMES \
    --env="XAUTHORITY=${XAUTH}" \
    --env="DISPLAY=${DISPLAY}" \
    --env="USER_ID=$USER_ID" \
    --privileged \
    --net=host \
    --ipc=host \
    $RUNTIME \
    ${IMAGE} \
    /bin/bash  -c  "while true; do echo hello world; sleep 1; done"