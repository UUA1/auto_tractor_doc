# 快速使用

### 安装 自动驾驶农机(ros)软件  

```bash
# step 1.  创建容器
wget -qO - https://gitlab.com/UUA1/auto_tractor_doc/-/raw/main/source/quickstart/dev_install/bin/run_develop.sh | bash 

# step 2 

tar xzvf  auto_tractor-main.tar.gz -C ${HOME}/shared_dir/work/src

# step 3. 
docker exec -it base-develop bash

mkdir -p $HOME/.config/gnss 

tee $HOME/.config/gnss/config.yaml <<- 'EOF'
localcartesian_h0: 0.0
localcartesian_lat0: 0.0
localcartesian_lon0: 0.0
localcartesian_pitch: 0.0
localcartesian_roll: 0.0
localcartesian_yaw: 0.0
EOF

<!--
localcartesian_h0: 0.0
localcartesian_lat0: 0.0
localcartesian_lon0: 0.0
localcartesian_pitch: 0.0
localcartesian_roll: 0.0
localcartesian_yaw: 0.0
tf_ndt_gnss:
  orientation: {w: 0.0, x: 0.0, y: 0.0, z: 0.0}
  position: {x: 0.0, y: 0.0, z: 0.0}
-->

cd ~/shared_dir/work/src
catkin_make

# step 4 
# 仿真端先启动仿真 ，或者 启动实车(拖拉机)

source  ~/shared_dir/work//devel/setup.bash

roslaunch launch record_sim.launch  # 连接仿真测试
roslaunch launch record.launch      # 连接实车测试

# 结束后 Ctrl+C 退出 record


# step 5 
# 仿真端, F12 回到原点 , 或者 实车(拖拉机),开回原点

source  ~/shared_dir/work/devel/setup.bash

roslaunch launch run_sim.launch    # 仿真
roslaunch launch run.launch        # 实车
```
