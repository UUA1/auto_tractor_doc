#!/bin/sh

#Ubuntu|Debian
grep -E "Ubuntu|Debian" "/proc/version" > /dev/null
if [ $? -eq 0 ];then
	# 设置apt-mark hold
	/usr/bin/apt-mark showhold | grep -E "linux-headers-$(uname -r)|linux-image-$(uname -r)" > /dev/null
	if [ $? -ne 0 ];then
		/usr/bin/apt-mark hold linux-image-$(uname -r) linux-headers-$(uname -r) 1>/dev/null 2>&1
		/usr/bin/apt-mark hold linux-image-generic linux-headers-generic 1>/dev/null 2>&1
	fi

	# 获取需要限制 dpkg 安装升级的软件列表
	dpkg --get-selections | grep -E "linux-headers|linux-image|linux-modules" | awk -F " " '{printf $1 "\n"}' | sort | uniq > "/tmp/ignore_update.list"
	# 逐行设置不升级
	while read line
	do
		echo $line" hold" | dpkg --set-selections
	done < "/tmp/ignore_update.list"
	# 删除临时文件
	rm -rf "/tmp/ignore_update.list"

	# 禁用 apt 自动升级
	if [ -f "/etc/apt/apt.conf.d/10periodic" ]; then
		grep -E "APT::Periodic::Update-Package-Lists \"1\";" "/etc/apt/apt.conf.d/10periodic" > /dev/null
		if [ $? -eq 0 ];then
			sed -i '/^APT::Periodic::Update-Package-Lists.*/cAPT::Periodic::Update-Package-Lists \"0\";' "/etc/apt/apt.conf.d/10periodic" 1>/dev/null 2>&1
		fi
	fi

	if [ -f "/etc/apt/apt.conf.d/20auto-upgrades" ]; then
		grep -E "APT::Periodic::Update-Package-Lists \"1\";" "/etc/apt/apt.conf.d/20auto-upgrades" > /dev/null
		if [ $? -eq 0 ];then
			sed -i '/^APT::Periodic::Update-Package-Lists.*";/cAPT::Periodic::Update-Package-Lists \"0\";' "/etc/apt/apt.conf.d/20auto-upgrades" 1>/dev/null 2>&1
		fi
	fi
fi

#Red Hat|NeoKylin
grep -E "Red Hat|NeoKylin" "/proc/version" > /dev/null
if [ $? -eq 0 -a -f /etc/yum.conf ];then
	grep -E "exclude=kernel*" "/etc/yum.conf" > /dev/null
	if [ $? -ne 0 ];then
		sed -i '/\[main\]/a\exclude=kernel*' /etc/yum.conf 1>/dev/null 2>&1
	fi
	grep -E "exclude=centos-release*" "/etc/yum.conf" > /dev/null
	if [ $? -ne 0 ];then
		sed -i '/\[main\]/a\exclude=centos-release*' /etc/yum.conf 1>/dev/null 2>&1
	fi
fi
