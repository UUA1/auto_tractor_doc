# 环境搭建手册-x86-Docker

## 系统安装

* 镜像下载 ： ubuntu-20.04.4 
    * [ubuntu-20.04.4-desktop-amd64.iso.torrent](http://releases.ubuntu.com/releases/20.04/ubuntu-20.04.4-desktop-amd64.iso.torrent)

* [使用 UltralISO 刻录启动盘](https://blog.csdn.net/u013894834/article/details/53982691)

* 如何安装Ubuntu系统-去百度找一篇文章，对着步骤操作即可。

------------

* 安装完毕后，更新,并禁用内核升级

    ```sh
     

    sudo apt-get install linux-image-$(uname -r) \
        linux-headers-$(uname -r)

    sudo apt-mark hold linux-image-$(uname -r)

    sudo apt-get install linux-modules-extra-$(uname -r) # depends can-dev.ko

    wget -qO - https://gitlab.com/UUA1/auto_tractor_doc/-/raw/main/source/development_environment/disable_kernel_update.sh | sudo  bash

    sudo shutdown -r now  # 重启
    ```

* [禁用自动更新](https://ubuntuqa.com/article/10986.html)

* 更新系统源地址

```bash
sudo sed -i 's/cn.archive.ubuntu.com/mirrors.aliyun.com/' /etc/apt/sources.list # X86 中文系统语言

sudo sed -i 's/archive.ubuntu.com/mirrors.aliyun.com/' /etc/apt/sources.list    # X86 英文系统语言
```

## nvidia dirver 安装

```bash

sudo apt-get remove --purge nvidia*

# For ubuntu 18.04
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys  F60F4B3D7FA2AF80

sudo add-apt-repository "deb https://mirrors.aliyun.com/nvidia-cuda/ubuntu1804/x86_64/ /"

sudo apt update

# 查看
ubuntu-drivers devices

# 在 third-party non-free版本中，选择最新的安装
# 比如当前，最新版本是  nvidia-driver-510
# 或者 , 直接 sudo apt-get install cuda  也可以
sudo apt-get install nvidia-driver-510

# 重启机器
sudo shutdown -r now

```

执行 `nvidia-smi` 查看有没有安装成功


## Nvidia Docker 

*  Docker  安装

```bash
wget -qO - https://gitee.com/my_shell/linux_install_shell/raw/master/docker/docker_ubuntu.sh | bash
```

* Nvidia Docker

```sh
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
   && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - \
   && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list


sudo apt-get update

sudo apt-get install -y nvidia-docker2

sudo systemctl restart docker
```

```sh
sudo tee /etc/docker/daemon.json <<-'EOF'
{
    "registry-mirrors": ["https://docker.mirrors.ustc.edu.cn"],

    "dns": ["114.114.114.114","8.8.8.8"],
    "runtimes": {
        "nvidia": {
            "path": "nvidia-container-runtime",
            "runtimeArgs": []
        }
    }
}
EOF

sudo apt-get install fonts-wqy-zenhei

sudo shutdown -r now  # 重启系统
```


## Can卡驱动 安装


* 安装 Peak Can Drivers

    <!--
    * [peak-linux-driver-8.10.2.tar.gz](https://www.peak-system.com/fileadmin/media/linux/files/peak-linux-driver-8.10.2.tar.gz)
    -->

    [peak-linux-driver-8.13.0.tar.gz](https://www.peak-system.com/fileadmin/media/linux/files/peak-linux-driver-8.13.0.tar.gz)


    ```sh
    # 1. install depends
    sudo apt-get install linux-headers-`uname -r`  \
                    libpopt-dev g++

    # 2. build
    tar -xzf peak-linux-driver-X.Y.Z.tar.gz
    cd peak-linux-driver-X.Y.Z

    make
    make -C driver netdev  # 支持 socketcan, 不支持 usbcan
    # make -C driver       # 支持 usbcan  ,但不支持 socketcan
    make -C lib
    make -C test
    make -C libpcanbasic


    # 3. install
    sudo make -C driver install

    sudo make -C lib install

    sudo make -C test install

    sudo make -C libpcanbasic install

    ## 如何卸载 
	# sudo make uninstall

    ip l | grpe can

    # 4.0 加载 peak_usb
    sudo modprobe peak_usb # 卸载 sudo modprobe -r peak_usb

    #####################################
    #  插上 usb peak can 进行测试 
    #####################################

    # 5.  set can0  up  
    sudo ip link set can0 up type can bitrate 500000

    # 6.  test send
    sudo apt-get install can-utils

    cansend can0 123#0102030405060708
    cangen -v can0   # 随机生成can消息
    candump can0

    ```

<!--
    ip -details -statistics link show can0
-->

* 串口配置

    ```sh
    sudo usermod -a -G dialout $USER
    sudo chmod a+wrx /dev/ttyS*
    ```



### 安装仿真LGSVL

* 配置

![img](img/03.png)

![img](img/04.png)

![img](img/05.png)


### 开发工具

* vscode 
* vscode 插件 : `Remote - Containers`
